<?php
function tentukan_nilai(int $number)
{
    if(100 >= $number && 85 <= $number) {
        $message = 'Sangat Baik';
    } else if(85 >= $number && 70 <= $number) {
        $message = 'Baik';
    } else if(70 >= $number && 60 <= $number) {
        $message = 'Cukup';
    } else {
        $message = 'Kurang';
    }

    return $message . '<br/>';
}

//TEST CASES
echo tentukan_nilai(85); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>