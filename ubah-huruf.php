<?php
function ubah_huruf(String $string){
    $alphabet = array_merge(range('A', 'Z'), range('a', 'z'));
    
    $newString = '';
    for($i=0;$i<strlen($string);$i++)
    {
        for($j=0;$j<count($alphabet);$j++)
        {
            if($string[$i] == $alphabet[$j])
            {
                if(in_array($j+1, [26,52])) $x=($j+1)-26;
                else $x=$j+1;

                $newString .= $alphabet[$x];
            }
        }
    }

    return $newString . '<br/>';
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>